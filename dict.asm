; В файле dict.asm создать функцию find_word. Она принимает два аргумента:
; Указатель на нуль-терминированную строку.
; Указатель на начало словаря.
; find_word пройдёт по всему словарю в поисках подходящего ключа.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения),
; иначе вернёт 0.


%define SIZE 8
extern string_equals

global find_word

section .text

; Функция ищёт ключ в словаре и, если найден, то возвращает адрес начала, иначе вернёт 0.
; Функция принимает указатель на нуль-терминированную строку и указаткль на начало словаря.
; rdi - указатель на строку
; rsi - указатель на начало словаря
; rax - ноль или начало строки-значения
find_word:
    .loop:
        add     rsi, SIZE
        push    rdi
        push    rsi
        call    string_equals
        pop     rsi
        pop     rdi
        cmp     rax, 1
        je      .found
        mov     rsi, [rsi - SIZE]
        cmp     rsi, 0
        jne     .loop
        jmp     .end

    .found:
        sub     rsi, SIZE
        mov     rax, rsi
        ret

    .end:
        xor     rax, rax
        ret
