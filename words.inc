; Файл words.inc должен хранить слова, определённые с помощью макроса colon.
; Включите этот файл в main.asm.

%include 'colon.inc'


colon 'd', d
db 'hello world 4', 0
colon 'c', c
db 'hello world 3', 0
colon 'b', b
db 'hello world 2', 0
colon 'a', a
db 'hello world 1', 0
