ASM=nasm 
ASM_FLAGS=-felf64 
LD=ld


all:
	@echo "build				- Собрать программу"
	@echo "clear				- Удалить все объектные файлы"
	@echo "run  				- Запустить программу"
	@exit 0


%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<


build: main.o lib.o dict.o
	$(LD) -o program $?


clear:
	rm -rf program *.o


run: build
	./program

.PHONY: all build run clear
