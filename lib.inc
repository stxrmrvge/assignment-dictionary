section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:  
    mov rax, 60		; Записываем код системного вызова exit в rax
    xor rdi, rdi		; Обнуляем флаги, очищаем регистр rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor	rax, rax
	.loop:
		cmp 	byte [rdi + rax], 0	; проверяем символ на нуль
		je  	.return			; если нуль, выходим из функции
		inc	rax
		jmp	.loop
	.return:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length			; смотрим на длину строки
	pop rdi
	mov	rdx, rax			; сохраняем в rdx (rdx - длина строки в syscall write)
	mov	rax, 1				; write syscall
	mov	rsi, rdi			; написываем адрес строки в rsi
	mov	rdi, 1				; stdout дескриптор
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push	rdi				; записываем в стек адрес символа
	mov	rsi, rsp			; записываем адрес вершины стека в rsi
	mov	rax, 1				; write syscall
	mov	rdi, 1				; stdout
	mov	rdx, 1				; длина строки (в нашем случае символ)
	syscall
	pop 	rdi				; достаем из стека адрес символа
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov	rdi, 0xA			; 0xA символ переносва строки
	call	print_char
	syscall
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov 	rax, rdi
	mov 	r8, 10				; делитель
	push	0				; поместим на вершину стека нуль-символ

	.loop:
		xor	rdx, rdx		
		div	r8			; делим rax на r8 (10)
		add	rdx, 0x30		; добавляем к числу 0x30, чтобы получить ASCII символ	
		push	rdx			; кидаем символ на вершину стека
		cmp	rax, 0			; сравниваем rax с нуль-символом
		jne	.loop

	.print_uint64:
		pop	rdi			; записываем символы в stdout пока не наткнемся
		cmp	rdi, 0			; на нуль-символ
		je	.return
		push 	rdi
		call	print_char
		pop 	rdi
		jmp	.print_uint64

	.return:
		ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test	rdi, rdi			; проверяем знак числа
	jns	.uint				; если положительное, переходим сразу к принту
	neg	rdi				; если нет, тогда меняем знак на противоположный
	mov	r8, rdi
	mov	rdi, '-'
	push	rdi
	push	r8
	call	print_char			; выводим "-" в stdout
	pop	r8
	pop	rdi
	mov	rdi, r8				; затем в rdi записываем число, и вызываем print_uint

	.uint:
		call	print_uint
		ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

; rdi - первая строка
; rsi - вторая строка
string_equals:
	xor	rcx, rcx
	.loop:
		mov	r8b, byte [rsi + rcx]		; сравниваем два символа, если они разные
		cmp	r8b, byte [rdi + rcx]		; тогда возвращаем 0
		jne	.return_zero

		cmp	byte [rsi + rcx], 0x0		; так как два символа одинаковых, мы можем проверить
		je	.return				        ; только один из них на нуль-символ, если это нуль
							            ; тогда возвращаем 
		inc	rcx
		jmp	.loop

	.return:
		mov	rax, 0x1
		ret

	.return_zero:
		xor	rax, rax
		ret
		

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push	0		; записываем нуль-символ на вершину стека
	mov	rax, 0		; read syscall
	mov	rdi, 0		; stdin дескриптор
	mov	rsi, rsp	; записываем вершину стека в rsi
	mov	rdx, 1		; длина читаемых байтов
	syscall
	pop	rax		    ; достаем символ из rax
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

; rdi - адрес начала буфера
; rsi - размер буфера
read_word:
	xor	rcx, rcx			    ; очищаем регистр rcx
	.loop:
		cmp	rcx, rsi		    ; сравниваем количество прочитанных символов и размер буфера
		jae	.overflow		    ; если символов больше/равно чем размер буфера, тогда выходим
		jmp	.save_regs
	.brc:					    ; before_read_char
		call	read_char		
		jmp	.pop_regs		    ; восстанавливаем регистры после вызова функции read_char
	.arc:					    ; after_read_char
		jmp	.check_whitespace	; проверяем символ (является ли он пробельным символом)
	.c_loop:				    ; continue_loop (продолжаем выполнение цикла
		mov	[rdi + rcx], rax	; записываем прочитанный символ в буфер
		inc	rcx
		jmp	.loop

	.overflow:
		xor	rax, rax		    ; возвращаем 0
		ret

	.save_regs:
		push	rdi
		push	rsi
		push	rdx
		push	rcx
		jmp	.brc

	.pop_regs:
		pop	rcx
		pop	rdx
		pop	rsi
		pop	rdi
		jmp	.arc

	.check_whitespace:			; проверка символа (является ли он пробельным)
		cmp	rax, 0x20
		je	.whitespace
		cmp	rax, 0x9
		je	.whitespace
		cmp	rax, 0x0		    ; если 0, тогда выходим из функции
		je	.return
		jmp 	.c_loop

	.whitespace:
		cmp	rcx, 0x0
		je	.loop

	.return:
		mov	[rdi + rcx], byte 0x0	; записываем в конец строки нуль-символ
		mov	rax, rdi
		mov	rdx, rcx
    		ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor	rcx, rcx
	xor	rax, rax
	xor	r8, r8
	mov	r9, 10
	.loop:
		mov	r8b, byte [rdi + rcx]		; записываем символ из строки в r8
		jmp	.is_digit
	.c_loop:
		sub	r8b, 0x30			        ; отнимаем '0' от символа, чтобы получить число из ASCII
		mul	r9				            ; умножаем rax на 10, для смещения
		add	rax, r8				        ; добавляем число к rax
		inc	rcx
		jmp	.loop

	.is_digit:
		cmp	r8, 0				; смотрим является ли нуль-символом
		je	.return
		cmp	r8, 0x30			; меньше ASCII символа '0'
		jb	.return
		cmp	r8, 0x39			; либо больше ASCII символа '9'
		ja	.return				; если да, выходим из функции
		jmp	.c_loop				; иначе продолжаем выполнение цикла
	
	.return:
		mov	rdx, rcx
		ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp		byte [rdi], '-'			; сравниваем первый символ с минусом
	jne		.uint				    ; если не минус, тогда сразу идем в функцию parse_uint

	inc		rdi				        ; иначе берем следующий символ
	push		rdi
	call		parse_uint			; парсим данное число без знака минуса
	pop		rdi
	inc		rdx				        ; инкрементируем длину строки, так как добавили '-'
	neg		rax				        ; изменяем знак rax
	ret

	.uint:
    	call	parse_uint
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длина буфера
string_copy:
	push	r8
	push	rcx
	push	rdi
	push	rsi
	push	rdx
	call	string_length				; записываем длину строки в rax
	pop	rdx
	pop	rsi
	pop	rdi
	pop	rcx
	pop	r8
	cmp	rax, rdx				        ; сравниваем длину строки и длину буфера
	jg	.return_zero

	mov	rcx, rax
	.loop:
		mov	r8b, byte [rdi + rcx]		; переносим символ строки в r8
		mov	byte [rsi + rcx], r8b		; затем переносим из r8 в буфер
		dec	rcx

		cmp	rcx, 0				        ; сравниваем rax и 0.
		jl	.return				        ; если меньше 0, тогда выходим
		jmp	.loop				        ; иначе продолжаем

	.return_zero:
		xor	rax, rax
		ret

	.return:
		ret
