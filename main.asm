; В main.asm определите функцию _start, которая:
; 
; Читает строку размером не более 255 символов в буфер с stdin.
; Пытается найти вхождение в словаре; если оно найдено, распечатывает в stdout значение по этому ключу.
; Иначе выдает сообщение об ошибке.
; 
; Не забудьте, что сообщения об ошибках нужно выводить в stderr.

%include 'words.inc'

%define ENDL 0xA
%define BUFFER_SIZE 256
%define SIZE 8

section .data
buffer: times BUFFER_SIZE db 0

input_key: db 'Enter a key:', ENDL, 0
key_too_long_error: db 'Key is too big!', ENDL, 0
found_value: db 'Value:', ENDL, 0
key_not_found_error: db 'This key is not in the dictionary!', ENDL, 0


global _start

extern print_string
extern print_newline
extern read_word
extern find_word
extern exit

section .text

_start: 
        mov     rdi, input_key
        call    print_string

        mov     rdi, buffer
        mov     rsi, BUFFER_SIZE
        call    read_word

        cmp     rax, 0
        je      .print_key_too_long_error

        push    rdx             ; Сохраняем длину ключа
        mov     rdi, buffer     ; Записываем ключ в буфер
        mov     rsi, top        ; Записываем конец словаря
        call    find_word

        cmp     rax, 0
        je      .print_key_not_found_error

        pop     rdx
        add     rax, SIZE
        add     rax, rdx
        inc     rax
        push    rax

        mov     rdi, found_value
        call    print_string
        pop     rdi
        call    print_string
        call    print_newline
        jmp     .end

    .print_key_not_found_error:
        mov     rdi, key_not_found_error
        call    print_string
        jmp     .end

    .print_key_too_long_error:
        mov     rdi, key_too_long_error
        call    print_string

    .end:
        mov     rdi, 0
        call    exit
